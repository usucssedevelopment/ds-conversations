﻿using GeoServer.Conversations;
using Messages;

namespace GeoServer
{
    public class ConversationFactory : CommSub.ConversationFactory
    {
        public override void Initialize()
        {
            Add(typeof(ComputeLandUseRequest), typeof(ComputeLandUseResponderConversation));
        }
    }
}
