﻿using System;
using System.Net;
using System.Net.Sockets;

using CommSub;
using Messages;

namespace GeoServer.Conversations
{
    public class ComputeLandUseResponderConversation : ResponderConversation
    {
        protected override void ExecuteDetails(object context)
        {
            var request = IncomingEnvelope?.Message as ComputeLandUseRequest;
            if (request == null) return;

            var listener = new TcpListener(new IPEndPoint(IPAddress.Any, 0));
            var port = ((IPEndPoint) listener.LocalEndpoint).Port;

            var response = new DataChannelResponse() {ConversationId = request.ConversationId, Port = port};
            var env = new Envelope() {Message = response, EndPoint = IncomingEnvelope.EndPoint};
            CommSubsystem.UdpCommunicator.Send(env);

            var tcpClient = listener.AcceptTcpClient();

            // TODO:
            //      Read 4 bytes from the tcpClient and interpret the number of bytes to read for the image
            //      Read that many byte from the tcpClient
            //      Convert to an image
            //
            //      Start computing the land-use map
            //      Every 1 second send back a status message
            //      End the computation is done, send back a final status message
            //
            //      Convert the land-use map to bytes
            //      Send the number of bytes as a 4 byte value
            //      Send out that many bytes
            //
            //      Be sure to handle errors, like Timeouts and the channel closing
            //

            tcpClient.Close();
        }
    }
}
