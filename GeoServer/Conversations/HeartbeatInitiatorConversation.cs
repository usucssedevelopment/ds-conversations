﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using CommSub;
using Messages;

namespace GeoServer.Conversations
{
    public class HeartbeatInitiatorConversation : InitiatorConversation
    {
        protected override Message CreateFirstMessage()
        {
            return new HeartbeatRequest() {MessageNumber = ConvId, ConversationId = ConvId};
        }

        public delegate void ProcessAliveHandler(IPEndPoint endPoint);
        public ProcessAliveHandler ProcessAliveCallback { get; set; }

        protected override Type[] ExceptedReplyType => new [] { typeof (Acknowledgement) };

        protected override void ProcessValidResponse(Envelope env)
        {
            ProcessAliveCallback?.Invoke(RemoteEndPoint);
        }
    }
}
