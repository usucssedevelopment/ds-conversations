﻿using CommSub;
using Messages;

namespace GeoClient.Conversations
{
    public class HeartbeatResponderConversation : ResponderConversation
    {
        protected override void ExecuteDetails(object context)
        {
            var acknowledgement = new Acknowledgement();
            var envelope = new Envelope() {Message = acknowledgement, EndPoint = RemoteEndPoint};
            if (!CommSubsystem.UdpCommunicator.Send(envelope))
                Error = "Cannot send back acknowledgement";
        }
    }
}
