﻿using System;
using System.Drawing;
using System.Net;
using System.Net.Sockets;
using log4net;

using CommSub;
using Messages;
using SharedObjects;

namespace GeoClient.Conversations
{
    public class ComputeLandUseInitiatorConversation : InitiatorConversation
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Conversation));

        public Image SatelliteImage { get; set; }
        public GeoRectangle BoundingGeoRectangle { get; set; }
        public Image LandUseMap { get; set; }

        protected override Message CreateFirstMessage()
        {
            return new ComputeLandUseRequest() { MessageNumber = ConvId, ConversationId = ConvId, BoundingGeoRectangle = BoundingGeoRectangle };
        }

        protected override Type[] ExceptedReplyType => new [] { typeof(DataChannelResponse), typeof(StatusMessage) };

        protected override void ProcessValidResponse(Envelope env)
        {
            var dataChannelResponse = env?.Message as DataChannelResponse;
            if (dataChannelResponse == null)
            {
                Error = "Invalid response from server";
                return;
            }

            TcpClient tcpClient = null;
            try
            {
                var tcpListenerEndPoint = new IPEndPoint(RemoteEndPoint.Address, dataChannelResponse.Port);
                tcpClient = new TcpClient();
                tcpClient.Connect(tcpListenerEndPoint);

                // TODO: 
                //
                //      Encode the image into bytes
                //      Send out 4 bytes which indicate the number of bytes
                //      Send those bytes out the tcpClient
                //

                var computationDone = false;
                var remainingRetries = MaxRetries;
                while (!computationDone && remainingRetries>0)
                {
                    if (IncomingEnvelopes.IsEmpty)
                        IncomingEvent.WaitOne(Timeout);

                    Envelope statusEnvelope = null;
                    var gotOne = IncomingEnvelopes.TryDequeue(out statusEnvelope);
                    if (gotOne && !IsEnvelopeValid(statusEnvelope, typeof (StatusMessage)))
                        statusEnvelope = null;

                    var statusMessage = statusEnvelope?.Message as StatusMessage;
                    if (statusMessage != null)
                    {
                        remainingRetries = MaxRetries;
                        computationDone = statusMessage.PercentComplete >= 100;
                    }
                    else
                        remainingRetries--;
                }

                if (!computationDone)
                {
                    Error = "Server started computation, but is no longer responding";
                    return;
                }

                //
                //      Read 4 bytes from the tcpClient and convert that to response length
                //      Read that many bytes from the tcpClient
                //      Convert those bytes to an land use map
                //
                //      Be sure to read with a timeout
                //

            }
            catch (SocketException ex)
            {
                // TODO: Handle socket exceptions appropriately
                //      Timeouts
                //      Channel closed by remote process
            }
            finally
            {
                tcpClient?.Close();
            }
        }
    }
}
