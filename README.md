# ds-conversations

This repository contains a two-program/two-protocol example of distributed application with a common communication subsystem.  It illustrates the use of conversation objects from a hierarchy of Conversation classes to manage exchanges between the two processes.  It also illustrates the use of the Factory Method and Strategy patterns, along with the application of the "Program to an Interface (or Abstraction)" and "Dependency Inversion" practices.
