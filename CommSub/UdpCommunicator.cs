﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

using Messages;
using SharedObjects;
using log4net;

namespace CommSub
{
    /// <summary>
    /// This class provides an abstraction for sending and receiving envelopes from a datagram socket (UdpClient).
    /// 
    /// An UdpCommnunicator is active in that it run an internal thread that grabs and queues incoming messages.
    /// </summary>
    public class UdpCommunicator
    {
        #region Private Data Members
        private ILog _logger;
        private ILog _loggerDeep;
        private UdpClient _myUdpClient;
        private Thread _receiveThread;
        private bool _started;
        private static readonly object StartStopLock = new object();
        private readonly ConcurrentQueue<Envelope> _incomingEnvelopes = new ConcurrentQueue<Envelope>();
        private readonly AutoResetEvent _waitHandle = new AutoResetEvent(false);         
        #endregion

        #region Public Properties
        public int MinPort { get; set; }
        public int MaxPort { get; set; }
        public int Timeout { get; set; }
        public int Port => ((IPEndPoint) _myUdpClient?.Client.LocalEndPoint)?.Port ?? 0;
        public string LoggerPrefix { get; set; }
        #endregion

        #region Public Methods
        /// <summary>
        /// This method starts up the UdpCommunicator by a) creating a UdpClient that is bound to a free port between the specified MinPort and MaxPort and b)
        /// starting a thread that looks for incoming messsages.  When that thread gets an incoming message, it will package it up in an envelope and callback
        /// to the specified EnvelopeHandler, which should be a method in the CommSubsystem
        /// </summary>
        public void Start()
        {
            LoggerPrefix = string.IsNullOrWhiteSpace(LoggerPrefix) ? "" : LoggerPrefix.Trim() + "_";

            _logger = LogManager.GetLogger(LoggerPrefix + typeof(UdpCommunicator));
            _loggerDeep = LogManager.GetLogger(LoggerPrefix + typeof(UdpCommunicator) + "_Deep");

            _logger.Info("Start communicator");

            ValidPorts();

            lock (StartStopLock)
            {
                if (_started) Stop();

                var portToTry = FindAvailablePort(MinPort, MaxPort);
                if (portToTry > 0)
                {
                    try
                    {
                        var localEp = new IPEndPoint(IPAddress.Any, portToTry);
                        _myUdpClient = new UdpClient(localEp);
                        _started = true;
                    }
                    catch (SocketException)
                    {
                    }
                }

                if (!_started)
                    throw new ApplicationException($"Cannot bind the socket to a port {portToTry}");
                else
                {
                    _receiveThread = new Thread(Run);
                    _receiveThread.Start();
                }
            }
        }

        /// <summary>
        /// This method stops the UdpCommunicator by a) closing its UdpClient and b) setting stopping the receive thread.
        /// </summary>
        public void Stop()
        {
            _logger.Debug("Entering Stop");

            lock (StartStopLock)
            {
                _started = false;
                _receiveThread?.Join(Timeout * 2);
                _receiveThread = null;

                if (_myUdpClient != null)
                {
                    _myUdpClient.Close();
                    _myUdpClient = null;
                }
            }

            _logger.Info("Communicator Stopped");
        }

        /// <summary>
        /// This method sends the message in an envelope to the EndPoint of the envelope.  Note that just the message is
        /// send out the UdpClient, not the whole envelope.
        /// </summary>
        /// <param name="outgoingEnvelope"></param>
        /// <returns></returns>
        public bool Send(Envelope outgoingEnvelope)
        {
            bool success = false;
            if (outgoingEnvelope == null || !outgoingEnvelope.IsValidToSend)
                _logger.Warn("Null or Invalid Envelope to Send");
            else
            {
                byte[] bytesToSend = outgoingEnvelope.Message.Encode();

                _logger.DebugFormat("Send out: {0} to {1}", Encoding.ASCII.GetString(bytesToSend), outgoingEnvelope.EndPoint);

                try
                {
                    _myUdpClient.Send(bytesToSend, bytesToSend.Length, outgoingEnvelope.EndPoint);
                    _loggerDeep.Debug("Send complete");
                    success = true;
                }
                catch (Exception err)
                {
                    _logger.Warn(err.Message);
                }
            }
            return success;
        }

        public Envelope Receive(int timeout)
        {
            Envelope env = null;
            var startTime = DateTime.Now;
            while (env == null && DateTime.Now.Subtract(startTime).TotalMilliseconds < timeout)
            {
                if (_incomingEnvelopes.IsEmpty)
                    _waitHandle.WaitOne(timeout);

                if (!_incomingEnvelopes.TryDequeue(out env))
                    env = null;
            }

            return env;
        }
        #endregion

        #region Private Methods
        private void Run()
        {
            while (_started)
            {
                var env = InternalReceive();
                if (env != null)
                {
                    _incomingEnvelopes.Enqueue(env);
                    _waitHandle.Set();
                }
            }
        }

        private Envelope InternalReceive()
        {

            IPEndPoint ep;
            var receivedBytes = ReceiveBytes(Timeout, out ep);
            if (receivedBytes == null || receivedBytes.Length <= 0) return null;

            Envelope result = null;
            var message = SerializableObject.Decode(receivedBytes) as Message;
            if (message != null)
            {
                result = new Envelope(message, ep);
                _logger.DebugFormat(@"Just received message, Nr={0}, Conv={1}, Type={2}, From={3}",
                    result.Message.MessageNumber?.ToString() ?? "null",
                    result.Message.ConversationId?.ToString() ?? "null",
                    result.Message.GetType().Name,
                    result.EndPoint?.ToString() ?? "null");
            }
            else
            {
                _logger.ErrorFormat($"Cannot decode message received from {ep}");
                _logger.ErrorFormat("bytes={0}", ConverBytesToDisplayString(receivedBytes));
            }

            return result;
        }

        private byte[] ReceiveBytes(int timeout, out IPEndPoint ep)
        {
            ep = null;
            if (_myUdpClient == null) return null;

            byte[] receivedBytes = null;
            _myUdpClient.Client.ReceiveTimeout = timeout;
            ep = new IPEndPoint(IPAddress.Any, 0);
            try
            {
                _loggerDeep.Debug("Try receive bytes from anywhere");
                receivedBytes = _myUdpClient.Receive(ref ep);
                _loggerDeep.Debug("Back from receive");

                if (_logger.IsDebugEnabled)
                    _logger.DebugFormat($"Incoming bytes={ConverBytesToDisplayString(receivedBytes)}");
            }
            catch (SocketException err)
            {
                if (err.SocketErrorCode != SocketError.TimedOut && err.SocketErrorCode != SocketError.Interrupted)
                    _logger.Warn(err.Message);
            }
            catch (Exception err)
            {
                _logger.Warn(err.Message);
            }
            return receivedBytes;
        }

        private void ValidPorts()
        {
            if ((MinPort != 0 && (MinPort < IPEndPoint.MinPort || MinPort > IPEndPoint.MaxPort)) ||
                (MaxPort != 0 && (MaxPort < IPEndPoint.MinPort || MaxPort > IPEndPoint.MaxPort)))
                throw new ApplicationException("Invalid port specifications");
        }

        private int FindAvailablePort(int minPort, int maxPort)
        {
            var availablePort = -1;

            _logger.DebugFormat("Find a free port between {0} and {1}", minPort, maxPort);
            for (var possiblePort = minPort; possiblePort <= maxPort; possiblePort++)
            {
                if (!IsUsed(possiblePort))
                {
                    availablePort = possiblePort;
                    break;
                }
            }
            _logger.DebugFormat("Available Port = {0}", availablePort);
            return availablePort;
        }

        private static bool IsUsed(int port)
        {
            var properties = IPGlobalProperties.GetIPGlobalProperties();
            var endPoints = properties.GetActiveUdpListeners();
            return endPoints.Any(ep => ep.Port == port);
        }

        private static string ConverBytesToDisplayString(byte[] bytes)
        {
            var builder = new StringBuilder();
            foreach (var b in bytes)
            {
                builder.Append(b.ToString().PadLeft(4));
            }
            return builder.ToString();
        }
        #endregion

    }
}
