﻿using log4net;
using Messages;

namespace CommSub
{
    /// <summary>
    /// ResponderConversation
    /// 
    /// In this system, a Responder Conversation is one that begin by responding to a request for a
    /// reliable request/reply.  It may do other things, but it must start with a response to a request.
    /// That request is placed in the IncomingEnvelope property.
    /// </summary>
    public abstract class ResponderConversation : Conversation
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ResponderConversation));

        /// <summary>
        /// For conversations started by an incoming message, this is the message
        /// </summary>
        public Envelope IncomingEnvelope { get; set; }

        protected override bool Initialize()
        {
            if (!base.Initialize()) return false;

            ConvId = IncomingEnvelope?.Message?.ConversationId;
            if (ConvId != null)
            {
                RemoteEndPoint = IncomingEnvelope?.EndPoint;
                State = PossibleState.Working;
            }
            else
                Error = $"Cannot initialize {GetType().Name} conversation because ConvId in incoming message is null";

            return (Error == null);
        }

    }
}
