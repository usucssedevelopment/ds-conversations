﻿using System.Collections.Concurrent;

using log4net;
using Messages;

namespace CommSub
{
    public class ConversationDictionary
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(ConversationDictionary));

        // Create a dictionary of queues for conversations in progress, plus a lock object for the dictionary
        private readonly ConcurrentDictionary<MessageId, Conversation> _activeConversation =
            new ConcurrentDictionary<MessageId, Conversation>(new MessageId.MessageIdComparer());

        public void Add(Conversation conversation)
        {
            if (conversation == null) return;
            Logger.Debug($"Try to add converstion {conversation.ConvId} to ConversationDictionary");

            var existingConversation = Lookup(conversation.ConvId);
            if (existingConversation == null)
                _activeConversation.TryAdd(conversation.ConvId, conversation);
        }

        public Conversation Lookup(MessageId convId)
        {
            Logger.Debug($"Lookup for conversation {convId}");

            Conversation conversation;
             _activeConversation.TryGetValue(convId, out conversation);

            return conversation;
        }

        public void Remove(MessageId convId)
        {
            Logger.DebugFormat("Remove Queue {0}", convId);
            Conversation conversation;
            _activeConversation.TryRemove(convId, out conversation);
        }

        public void ClearAll()
        {
            _activeConversation.Clear();
        }

        public int ConversationQueueCount => _activeConversation.Count;
    }
}
