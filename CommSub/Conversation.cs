﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading;
using System.Net;

using log4net;
using Messages;

namespace CommSub
{
    /// <summary>
    /// Conversation
    /// 
    /// This is the base abstraction for all conversations and as such, include a number of common properties for all conversations.
    /// Specifically, every conversation has reference to the communication subsystem, so it can access the Subsystem's UdpCommunicator
    /// and ConversationFactory.  It also keeps track of the Conversation Id given to it by the Conversation Factory.
    /// </summary>
    public abstract class Conversation
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Conversation));

        protected readonly ConcurrentQueue<Envelope> IncomingEnvelopes = new ConcurrentQueue<Envelope>();
        protected readonly AutoResetEvent IncomingEvent = new AutoResetEvent(false);

        public enum PossibleState
        {
            NotInitialized,
            Working,
            Failed,
            Successed
        };

        public PossibleState State { get; protected set; } = PossibleState.NotInitialized;

        /// <summary>
        /// This is CommSubsystem managing this conversation
        /// </summary>
        public CommSubsystem CommSubsystem { get; set; }
        public IPEndPoint RemoteEndPoint { get; set; }

        /// <summary>
        /// For conversations that will have a timeout, this is the timeout value in milliseconds
        /// </summary>
        public int Timeout { get; set; } = 3000;

        /// <summary>
        /// For conversation that can resend and retry the waiting for a reply, this is the maximum number of retries
        /// </summary>
        public int MaxRetries { get; set; } = 3;

        /// <summary>
        /// Hold the identitifier for this conversation's queue
        /// </summary>
        public MessageId ConvId { get; set; }

        public string Error { get; protected set; }

        /// <summary>
        /// This is set to true when the conversation finishes
        /// </summary>
        public bool Done { get; protected set; }

        /// <summary>
        /// Launches a conversation on its on threat
        /// </summary>
        /// <param name="context"></param>
        public void Launch(object context = null)
        {
            var result = ThreadPool.QueueUserWorkItem(Execute, context);
            Logger.DebugFormat("Launch of {0}, result = {1}", GetType().Name, result);
        }

        /// <summary>
        /// Execute
        /// 
        /// This method executes a conversation.  This is used by Launch, but can be called directly to run a conversation on the
        /// current thread.
        /// 
        /// This method is a template method that calls two abstract or virtual methods, namely Initialize and ExecuteDetails.
        /// Concrete specializations (or intermediate base classes) need to implement these methods.
        /// </summary>
        /// <param name="context"></param>
        public void Execute(object context = null)
        {
            if (Initialize())
                ExecuteDetails(context);

            if (string.IsNullOrEmpty(Error))
                State = PossibleState.Successed;
            else
            {
                State = PossibleState.Failed;
                Logger.Warn(Error);
            }
        }

        /// <summary>
        /// Process
        /// 
        /// This method processes an incoming envelope
        /// </summary>
        /// <param name="env"></param>
        public void Process(Envelope env)
        {
            if (env?.Message == null || env.EndPoint == null) return;

            IncomingEnvelopes.Enqueue(env);
            IncomingEvent.Set();
        }


        /// <summary>
        /// Initializes all computated state information for the conversation.  It should be called after the public
        /// properties for the conversation are setup.  For example, it should be called after the ConvId is set and
        /// if the conversation is a Initiator, after the RemoteEndPoint is set.  A specialization may override this
        /// method as needed, but the override should have as its first line the following:
        /// 
        ///     if (!base.Initialize()) return false;
        /// 
        /// </summary>
        /// <returns></returns>
        protected virtual bool Initialize()
        {
            State = PossibleState.Working;
            return true;
        }

        /// <summary>
        /// ExecuteDetails
        /// 
        /// This is called by Execute as one of the steps in that template method.  Concrete specialization (or intermediate
        /// classes) need to implement this method.
        /// </summary>
        /// <param name="context"></param>
        protected abstract void ExecuteDetails(object context);

        /// <summary>
        /// IsEnvelopeValid
        /// 
        /// This method checks to see is an envelope is not null, contains a message, and that the message has a conversation id.
        /// It also, checks to see if the messsage is the envelope is one of the expected types.
        /// </summary>
        /// <param name="env">The envelope to be validated</param>
        /// <param name="allowedTypes">The types of messages to allow in the envelope</param>
        /// <returns></returns>
        protected bool IsEnvelopeValid(Envelope env, params Type[] allowedTypes)
        {
            Error = null;
            Logger.Debug("Checking to see if envelope is valid and message of appropriate type");
            if (env?.Message == null)
                Error = "Null envelope";
            else if (env.Message.MessageNumber == null)
                Error =  "Null Message Number" ;
            else if (env.Message.ConversationId == null)
                Error = "Null Conversation Id";
            else
            {
                var messageType = env.Message.GetType();
                
                Logger.DebugFormat("See if {0} is valid for a {1} conversation", messageType, GetType().Name);
                if (!allowedTypes.Contains(messageType))
                    Error = $"Invalid Type of Message. Allow Types: {allowedTypes.Aggregate(string.Empty, (current, t) => current + t.ToString()) }";
            }

            if (Error!=null)
                Logger.Error(Error);

            return (Error==null);
        }

        /// <summary>
        /// DoReliableRequestReply
        /// 
        /// This method executes a reliable request/reply with Timeout and MaxRetries
        /// </summary>
        /// <param name="outgoingEnv"></param>
        /// <param name="allowedTypes"></param>
        /// <returns></returns>
        protected Envelope DoReliableRequestReply(Envelope outgoingEnv, params Type[] allowedTypes)
        {
            Envelope incomingEnvelope = null;

            var remainingSends = MaxRetries;
            while (remainingSends > 0 && incomingEnvelope == null)
            {
                remainingSends--;
                if (!CommSubsystem.UdpCommunicator.Send(outgoingEnv))
                {
                    Error = "Cannot send message";
                    break;
                }

                if (IncomingEnvelopes.IsEmpty)
                    IncomingEvent.WaitOne(Timeout);

                var gotOne = IncomingEnvelopes.TryDequeue(out incomingEnvelope);
                if (gotOne && !IsEnvelopeValid(incomingEnvelope, allowedTypes))
                    incomingEnvelope = null;
            }

            if (Error != null)
                Logger.Error(Error);

            return incomingEnvelope;
        }

    }
}
