﻿using System.Net;
using System.Net.NetworkInformation;
using log4net;

namespace CommSub
{
    /// <summary>
    /// CommSubsystem
    /// 
    /// A CommSubsystem is a facade that encapsulates a CommProcessState, UdpCommunication, QueueDictionary, and ConversationFactory
    /// </summary>
    public class CommSubsystem
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(CommSubsystem));

        private IPAddress _bestAddress;
        private readonly ConversationFactory _conversationFactory;
        private readonly int _minPort;
        private readonly int _maxPort;

        public int Port => UdpCommunicator?.Port ?? 0;
        public string BestLocalEndPoint => $"{FindBestLocalIpAddress()}:{Port}";
        public ConversationDictionary Conversations { get; }
        public UdpCommunicator UdpCommunicator { get; private set; }

        public CommSubsystem(ConversationFactory factory, int minPort, int maxPort)
        {
            _conversationFactory = factory;
            _conversationFactory.ManagingSubsystem = this;

            _minPort = minPort;
            _maxPort = maxPort;

            Conversations = new ConversationDictionary();
        }

        /// <summary>
        /// This methods setup up all of the components in a CommSubsystem.  Call this method
        /// sometime after setting the MinPort, MaxPort, and ConversationFactory
        /// </summary>
        public void Initialize()
        {
            _conversationFactory.Initialize();

            UdpCommunicator = new UdpCommunicator()
                                    {
                                        MinPort = _minPort,
                                        MaxPort = _maxPort,
                                        Timeout = 3000
                                    };
            UdpCommunicator.Start();
        }
        
        /// <summary>
        /// This method stops all of the active components of a CommSubsystem and release the
        /// releases (or at least allows them to be garabage collected.  Once stop is called,
        /// a CommSubsystem cannot be restarted with setting it up from scratch.
        /// </summary>
        public void Stop()
        {
            Logger.Debug("Entering Stop");

            if (UdpCommunicator != null)
            {
                UdpCommunicator.Stop();
                UdpCommunicator = null;
            }

            Logger.Debug("Leaving Stop");
        }

        private IPAddress FindBestLocalIpAddress()
        {
            if (_bestAddress != null) return _bestAddress;
            
            long bestPreferredLifetime = 0;
            var adapters = NetworkInterface.GetAllNetworkInterfaces();
            foreach (var adapter in adapters)
            {
                if (adapter.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
                    adapter.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
                {
                    foreach (var ip in adapter.GetIPProperties().UnicastAddresses)
                    {
                        if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                        {
                            if (_bestAddress == null || ip.AddressPreferredLifetime > bestPreferredLifetime)
                            {
                                _bestAddress = ip.Address;
                                bestPreferredLifetime = ip.AddressPreferredLifetime;
                            }
                        }
                    }
                }
            }
            return _bestAddress;
        }
    }
}
