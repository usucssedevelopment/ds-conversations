﻿using log4net;

namespace CommSub
{

    public abstract class AppProcess
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(AppProcess));

        protected const int MainLoopSleep = 200;
        protected object MyLock = new object();

        public CommSubsystem MyCommSubsystem { get; protected set; }

        protected virtual void SetupCommSubsystem(ConversationFactory conversationFactory, int minPort, int maxPort)
        {
            MyCommSubsystem = new CommSubsystem(conversationFactory, minPort, maxPort);
            MyCommSubsystem.Initialize();
        }

    }

}
