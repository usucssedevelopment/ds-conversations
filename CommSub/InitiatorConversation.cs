﻿using System;
using log4net;
using Messages;

namespace CommSub
{
    /// <summary>
    /// InitiatorConversation
    /// 
    /// In this system, an initiator conversation is one that starts with a Reliable Request reliable.  It may involve
    /// other messages, but it must start with a reliable request reply.
    /// 
    /// </summary>
    public abstract class InitiatorConversation : Conversation
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(InitiatorConversation));

        protected Envelope FirstEnvelope { get; set; }

        /// <summary>
        /// Initialize
        /// 
        /// This method is called by Execute and is itself a template method, meaning that it calls
        /// at least one abstract or virtual method that has to be implemented by concrete specializations.
        /// In this case, the CreateFirstMessage method is the method that concrete specialization have
        /// to implement.
        /// </summary>
        /// <returns></returns>
        protected override bool Initialize()
        {
            if (!base.Initialize()) return false;

            if (RemoteEndPoint == null)
            {
                Logger.Error("No remote end point set for an initiator conversation");
                return false;
            }

            FirstEnvelope = null;

            var msg = CreateFirstMessage();
            if (msg == null) return false;

            FirstEnvelope = new Envelope() {Message = msg, EndPoint = RemoteEndPoint};
            return true;
        }

        protected abstract Message CreateFirstMessage();

        /// <summary>
        /// Execute Details
        /// 
        /// This method is called by Execute and is itself a template method, meaning
        /// that it calls some abstract method that have be implemented about specialization
        /// of this class.
        /// </summary>
        /// <param name="context"></param>
        protected override void ExecuteDetails(object context)
        {
            var env = DoReliableRequestReply(FirstEnvelope, ExceptedReplyType);
            Logger.Debug("Back from DoReliableRequestReply with");

            if (env == null)
                Error = "No response received";
            else
                ProcessValidResponse(env);
        }

        protected abstract Type[] ExceptedReplyType { get; }

        protected abstract void ProcessValidResponse(Envelope env);
    }
}
