﻿namespace Messages
{
    public class Acknowledgement : Message
    {
        public static byte SerializationTypeCode { get; } = 10;
    }
}
