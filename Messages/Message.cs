﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SharedObjects;

namespace Messages
{
    public abstract class Message : SerializableObject
    {
        static Message()
        {
            AddSerializableType(Acknowledgement.SerializationTypeCode, typeof(Acknowledgement));
            AddSerializableType(ComputeLandUseRequest.SerializationTypeCode, typeof(ComputeLandUseRequest));
            AddSerializableType(DataChannelResponse.SerializationTypeCode, typeof(DataChannelResponse));
            AddSerializableType(HeartbeatRequest.SerializationTypeCode, typeof(HeartbeatRequest));
            AddSerializableType(StatusMessage.SerializationTypeCode, typeof(StatusMessage));
        }

        public MessageId MessageNumber { get; set; }
        public MessageId ConversationId { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            if (MessageNumber==null)
                MessageNumber = new MessageId();

            if (ConversationId == null)
                ConversationId = MessageNumber;

            memoryStream.Write(MessageNumber.Encode());
            memoryStream.Write(ConversationId.Encode());
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            memoryStream.ReadOneByte();            // Consume message type code
            var bytes = memoryStream.ReadBytes();
            MessageNumber = SerializableObject.Decode(bytes) as MessageId;
            bytes = memoryStream.ReadBytes();
            MessageNumber = SerializableObject.Decode(bytes) as MessageId;
        }
    }
}
