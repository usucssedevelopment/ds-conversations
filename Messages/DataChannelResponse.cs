﻿using System.IO;
using SharedObjects;

namespace Messages
{
    public class DataChannelResponse : Message
    {
        public static byte SerializationTypeCode { get; } = 12;

        public int Port { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            memoryStream.Write(Port);
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            Port = memoryStream.ReadInt();
        }
    }
}
