﻿using System;
using System.Collections.Generic;
using System.IO;
using SharedObjects;

namespace Messages
{
    public class MessageId : SerializableObject
    {
        private static short _nextSeqNumber;                           // Initialize to 0, which means it will start with message #1
        private static readonly object MyLock = new object();

        public short Pid { get; private set; }
        public short Seq { get; private set; }

        public MessageId()
        {
            Pid = LocalProcessInfo.Instance.ProcessId;
            Seq = GetNextSeqNumber();
        }

        public MessageId(short pid, short seq)
        {
            Pid = pid;
            Seq = seq;
        }

        public MessageId Clone()
        {
            return MemberwiseClone() as MessageId;
        }

        public override string ToString()
        {
            return Pid.ToString() + "." + Seq.ToString();
        }

        private static short GetNextSeqNumber()
        {
            lock (MyLock)
            {
                if (_nextSeqNumber == short.MaxValue)
                    _nextSeqNumber = 0;
                ++_nextSeqNumber;
            }
            return _nextSeqNumber;
        }

        public override int GetHashCode()
        {
            return (Convert.ToInt32(Pid) << 16) | (Convert.ToInt32(Seq) & 0xFFFF);
        }

        public override bool Equals(object obj)
        {
            return Compare(this, obj as MessageId) == 0;
        }

        public static int Compare(MessageId a, MessageId b)
        {
            if ((a == null && b == null) || ReferenceEquals(a, b)) return 0;
            if (a == null) return -1;
            if (b == null) return 1;
            if (a.Pid < b.Pid) return -1;
            if (a.Pid > b.Pid) return 1;
            if (a.Seq < b.Seq) return -1;
            return a.Seq > b.Seq ? 1 : 0;
        }

        public static bool operator ==(MessageId a, MessageId b)
        {
            return (Compare(a,b) == 0);
        }

        public static bool operator !=(MessageId a, MessageId b)
        {
            return (Compare(a,b) !=0 );
        }

        public static bool operator <(MessageId a, MessageId b)
        {
            return (Compare(a, b) < 0);
        }

        public static bool operator >(MessageId a, MessageId b)
        {
            return (Compare(a, b) > 0);
        }

        public static bool operator <=(MessageId a, MessageId b)
        {
            return (Compare(a, b) <= 0);
        }

        public static bool operator >= (MessageId a, MessageId b)
        {
            return (Compare(a, b) >= 0);
        }

        /// <summary>
        /// This is a class provide a comparer so MessageNumber can used as a dictionary key
        /// </summary>
        public class MessageIdComparer : IEqualityComparer<MessageId>
        {
            public bool Equals(MessageId id1, MessageId id2)
            {
                return id1 == id2;
            }

            public int GetHashCode(MessageId id)
            {
                return id.GetHashCode();
            }
        }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            memoryStream.Write(Pid);
            memoryStream.Write(Seq);
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            Pid = memoryStream.ReadShort();
            Seq = memoryStream.ReadShort();
        }
    }


}
