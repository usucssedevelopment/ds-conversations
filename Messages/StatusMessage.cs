﻿using System.IO;
using SharedObjects;

namespace Messages
{
    public class StatusMessage : Message
    {
        public static byte SerializationTypeCode { get; } = 14;

        public float PercentComplete { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            memoryStream.Write(PercentComplete);
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            PercentComplete = memoryStream.ReadFloat();
        }

    }
}
