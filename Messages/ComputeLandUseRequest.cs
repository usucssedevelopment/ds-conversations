﻿using System.IO;
using SharedObjects;

namespace Messages
{
    public class ComputeLandUseRequest : Message
    {
        public static byte SerializationTypeCode { get; } = 11;

        public GeoRectangle BoundingGeoRectangle { get; set; }

        protected override void InternalEncode(MemoryStream memoryStream)
        {
            base.InternalEncode(memoryStream);
            memoryStream.Write(BoundingGeoRectangle!=null);
            if (BoundingGeoRectangle!=null)
                memoryStream.Write(BoundingGeoRectangle.Encode());
        }

        protected override void InternalDecode(MemoryStream memoryStream)
        {
            base.InternalDecode(memoryStream);
            var containsBoundingRectangle = memoryStream.ReadBool();
            if (!containsBoundingRectangle)
            {
                BoundingGeoRectangle = null;
            }
            else
            {
                var bytes = memoryStream.ReadBytes();
                BoundingGeoRectangle = Decode(bytes) as GeoRectangle;
            }
        }
    }
}
