﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SharedObjects
{
    public class SerializableObject
    {
        private static byte _nextTypeId;
        private static readonly Dictionary<byte, Type> SerializableTypes;

        static SerializableObject()
        {
            _nextTypeId = 1;
            SerializableTypes = new Dictionary<byte, Type>();

            AddSerializableType(GeoRectangle.SerializationTypeCode, typeof(GeoRectangle));
            AddSerializableType(SatelliteImage.SerializationTypeCode, typeof(SatelliteImage));
            AddSerializableType(LandUseMap.SerializationTypeCode, typeof(LandUseMap));
        }

        protected static void AddSerializableType(byte typeCode, Type type)
        {
            if (!typeof(SerializableObject).IsAssignableFrom(type))
                throw new ApplicationException("Cannot add a serializable type that does not inherit from SerializableObject");

            if (SerializableTypes.ContainsKey(typeCode) && SerializableTypes[typeCode]!=type)
                throw new ApplicationException($"TypeCode {typeCode} is already assigned to type {type}");

            if (!SerializableTypes.ContainsKey(typeCode))
                SerializableTypes.Add(_nextTypeId++, type);
        }

        public byte[] Encode()
        {
            var memoryStream = new MemoryStream();
            InternalEncode(memoryStream);               // Call object's specific internal encode
            return memoryStream.ToArray();
        }

        public static SerializableObject Decode(byte[] bytes)
        {
            if (bytes == null || bytes.Length < 1)
                throw new ApplicationException("Cannot decode message");

            var objectType = FindSerializableType(bytes[0]);
            if (objectType == null)
                throw new ApplicationException("Unknown message type");

            var serializableObject = Activator.CreateInstance(objectType) as SerializableObject;
            if (serializableObject == null)
                throw new ApplicationException($"Cannot create an object of type {objectType}");

            var memoryStream = new MemoryStream(bytes);
            serializableObject.InternalDecode(memoryStream);

            return serializableObject;
        }

        protected virtual void InternalEncode(MemoryStream memoryStream)
        {
            memoryStream.Write(FindSerializableTypeCode(this.GetType()));
        }

        protected virtual void InternalDecode(MemoryStream memoryStream)
        {
            memoryStream.ReadOneByte();            // Consume message type code
        }

        private static byte FindSerializableTypeCode(Type type)
        {
            return SerializableTypes.Where(entry => entry.Value == type).Select(entry => entry.Key).FirstOrDefault();
        }

        private static Type FindSerializableType(byte code)
        {
            return SerializableTypes.ContainsKey(code) ? SerializableTypes[code] : null;
        }

    }
}
