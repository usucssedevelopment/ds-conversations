﻿using System;
using System.IO;
using System.Net;
using System.Text;

namespace SharedObjects
{
    public static class StreamExtension
    {
        public static void Write(this Stream memoryStream, bool value)
        {
            memoryStream.WriteByte(value ? (byte) 1 : (byte) 0);
        }

        public static void Write(this Stream memoryStream, byte value)
        {
            memoryStream.WriteByte(value);
        }

        public static void Write(this Stream memoryStream, short value)
        {
            var bytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        public static void Write(this Stream memoryStream, int value)
        {
            var bytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        public static void Write(this Stream memoryStream, long value)
        {
            var bytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(value));
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        public static void Write(this Stream memoryStream, float value)
        {
            var bytes = BitConverter.GetBytes(value);
            bytes = OrderByArray(bytes);
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        public static void Write(this Stream memoryStream, string value)
        {
            if (value == null)
                value = string.Empty;

            var bytes = Encoding.BigEndianUnicode.GetBytes(value);
            Write(memoryStream, (short)bytes.Length);
            memoryStream.Write(bytes, 0, bytes.Length);
        }

        public static void Write(this Stream memoryStream, byte[] bytes)
        {
            if (bytes == null)
                bytes = new byte[0];

            memoryStream.Write((short) bytes.Length);

            if (bytes.Length>0)
                memoryStream.Write(bytes, 0, bytes.Length);
        }

        public static bool ReadBool(this Stream stream)
        {
            var oneByte = stream.ReadByte();
            return oneByte == 1;
        }

        public static byte ReadOneByte(this Stream stream)
        {
            var oneByte = stream.ReadByte();
            if (oneByte == -1)
                throw new ApplicationException("Cannot read byte from input stream");
            return (byte)oneByte;
        }

        public static short ReadShort(this Stream stream)
        {
            var bytes = new byte[2];
            var bytesRead = stream.Read(bytes, 0, bytes.Length);
            if (bytesRead != bytes.Length)
                throw new ApplicationException("Cannot decode a short integer from message");

            return IPAddress.NetworkToHostOrder(BitConverter.ToInt16(bytes, 0));
        }

        public static int ReadInt(this Stream stream)
        {
            var bytes = new byte[4];
            var bytesRead = stream.Read(bytes, 0, bytes.Length);
            if (bytesRead != bytes.Length)
                throw new ApplicationException("Cannot decode an integer from message");

            return IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 0));
        }

        public static long ReadLong(this Stream stream)
        {
            var bytes = new byte[8];
            var bytesRead = stream.Read(bytes, 0, bytes.Length);
            if (bytesRead != bytes.Length)
                throw new ApplicationException("Cannot decode a long integer from message");

            return IPAddress.NetworkToHostOrder(BitConverter.ToInt64(bytes, 0));
        }

        public static float ReadFloat(this Stream stream)
        {
            var bytes = new byte[4];
            var bytesRead = stream.Read(bytes, 0, bytes.Length);
            if (bytesRead != bytes.Length)
                throw new ApplicationException("Cannot decode an integer from message");
            bytes = OrderByArray(bytes);
            return BitConverter.ToSingle(bytes, 0);
        }

        public static string ReadString(this Stream stream)
        {
            var result = string.Empty;
            var length = ReadShort(stream);
            if (length <= 0) return result;

            var bytes = new byte[length];
            var bytesRead = stream.Read(bytes, 0, bytes.Length);
            if (bytesRead != length)
                throw new ApplicationException("Cannot decode a string from message");

            result = Encoding.BigEndianUnicode.GetString(bytes, 0, bytes.Length);
            return result;
        }

        public static byte[] ReadBytes(this Stream stream)
        {
            var length = ReadShort(stream);
            if (length <= 0) return new byte[0];

            var bytes = new byte[length];
            var bytesRead = stream.Read(bytes, 0, bytes.Length);
            if (bytesRead != length)
                throw new ApplicationException($"Cannot decode {length} bytes from message");

            return bytes;
        }

        private static byte[] OrderByArray(byte[] bytes)
        {
            if (!BitConverter.IsLittleEndian) return bytes;

            var tmp = new byte[4];
            for (var i = 0; i < 4; i++)
                tmp[i] = bytes[3 - i];
            bytes = tmp;
            return bytes;
        }

    }
}
